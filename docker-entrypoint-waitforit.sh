#!/bin/bash
/wait-for-it.sh $WAIT_FOR_HOST_AND_PORT -t 60 -- /opt/jboss/tools/docker-entrypoint.sh $@
exit $?
