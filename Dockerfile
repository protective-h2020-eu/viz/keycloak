FROM jboss/keycloak

COPY docker-entrypoint-waitforit.sh wait-for-it.sh /
ADD protective-realm.json /tmp/
ADD ProtectiveTheme /opt/jboss/keycloak/themes/ProtectiveTheme

ENTRYPOINT ["/docker-entrypoint-waitforit.sh"]
CMD ["-b", "0.0.0.0"]
